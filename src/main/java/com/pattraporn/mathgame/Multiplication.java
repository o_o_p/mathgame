/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pattraporn.mathgame;

/**
 *
 * @author Pattrapon N
 */
public class Multiplication extends AllSymbolMath{
   private double s ;
  private double r ;
    
    public Multiplication(double s,double r){
        this.s =s;
        this.r =r;
        System.out.println("Multiplication sign math problem");
    }  
    @Override
    public double Answer(){
        return s*r;
    } 
}
