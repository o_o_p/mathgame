/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pattraporn.mathgame;
import java.util.Scanner;
/**
 *
 * @author Pattrapon N
 */
public class Plus extends AllSymbolMath{
  Scanner mk = new Scanner(System.in);
  private double num1 ;
  private double num2 ;
    
    public Plus(double num1,double num2){
       
       
        this.num1 =num1;
        this.num2 =num2;
        System.out.println("plus sign math problem");
    }  
    @Override
    public double Answer(){
        return num1+num2;
    }
}
