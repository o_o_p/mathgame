/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pattraporn.mathgame;

/**
 *
 * @author Pattrapon N
 */
public class Division extends AllSymbolMath{
    private double q;
    private double w;

    public Division(double q, double w) {
        this.q = q;
        this.w = w;
        System.out.println("Division sign math problem");
    }

    @Override
    public double Answer() {
        return q / w;
    }
}
