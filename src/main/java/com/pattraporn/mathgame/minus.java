/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pattraporn.mathgame;

/**
 *
 * @author Pattrapon N
 */
public class minus extends AllSymbolMath {

    private double a;
    private double b;

    public minus(double a, double b) {
        this.a = a;
        this.b = b;
        System.out.println("minus sign math problem");
    }

    @Override
    public double Answer() {
        return a - b;
    }
}
